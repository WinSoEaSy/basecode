package fptedu.swp391_gr2_se1736.swp391pj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fptedu.swp391_gr2_se1736.swp391pj.dto.UsersDTO;
import fptedu.swp391_gr2_se1736.swp391pj.model.Users;
import fptedu.swp391_gr2_se1736.swp391pj.respository.UsersRespository;

@Service
public class UserService {
    
    @Autowired
    private UsersRespository usersRespository;

    public UsersDTO getLoginUserInfo(String userName, String password){
        Users loginUser = usersRespository.findUsersByUserNameAndPassword(userName, password);
        UsersDTO userInfo = new UsersDTO(loginUser);
        return userInfo;
    }

    public UsersDTO getUserById(Integer id){
        Users user = usersRespository.getUsersByUserID(id);
        UsersDTO udto = new UsersDTO(user);
        return udto;
    }
}
