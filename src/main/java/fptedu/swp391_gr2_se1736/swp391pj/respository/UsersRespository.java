package fptedu.swp391_gr2_se1736.swp391pj.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fptedu.swp391_gr2_se1736.swp391pj.model.Users;





@Repository
public interface UsersRespository extends JpaRepository<Users, Integer>{

    Users findUsersByUserNameAndPassword(String userName, String password);

    Users getUsersByUserID(Integer userID);


}
