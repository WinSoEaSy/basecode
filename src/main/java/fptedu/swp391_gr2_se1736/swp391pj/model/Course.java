package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Course {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CourseID")
    private Integer courseId;
    @Column(name = "CourseName")
    private String courseName;
    @Column(name = "CreateDate")
    private Date createDate;
    @Column(name = "CoursePrice")
    private Double coursePrice;
    @Column(name = "Description")
    private String description;

    @Lob
    @Column(columnDefinition = "BLOB", name = "Image")
    private byte[] image;
    @Column(name = "Rating")
    private Integer rating;
    @Column(name = "CourseStatus")
    private Integer courseStatus;

    @Column(name = "CategoryID")
    private Integer categoryId;
    @Column(name = "CourseOwnerID")
    private Integer courseOwnerId;
}
