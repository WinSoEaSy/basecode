package fptedu.swp391_gr2_se1736.swp391pj.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "QuestionID")
    private Integer questionId;
    @Column(name = "Content")
    private String content;
    @Column(name = "QuestionOrder")
    private Integer questionOrder;

    @Column(name = "QuizID")
    private Integer quizId;
}
