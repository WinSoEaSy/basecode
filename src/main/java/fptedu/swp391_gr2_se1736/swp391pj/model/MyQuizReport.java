package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class MyQuizReport {
    @Column(name = "Result")
    private Double result;
    @Column(name = "ReprotDOB")
    private Date reprotDob;

    @Column(name = "UserID")
    private Integer userId;
    @Column(name = "QuizID")
    private Integer quizId;
}
