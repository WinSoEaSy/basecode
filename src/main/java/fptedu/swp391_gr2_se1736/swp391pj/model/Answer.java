package fptedu.swp391_gr2_se1736.swp391pj.model;

import jakarta.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Answer {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "AnswerID")
    private Integer answerId;
    @Column(name = "Content")
    private String content;
    @Column(name = "Correct")
    private boolean correct;
    @Column(name = "AnswerOrder")
    private Integer answerOrder;
}
