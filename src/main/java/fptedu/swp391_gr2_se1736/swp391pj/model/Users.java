package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table
public class Users {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="UserID")
    private Integer userID;
    @Column(name="FullName")
    private String fullName;
    @Column(name="Email")
    private String email;
    @Column(name="Gender")
    private boolean gender;
    @Column(name="Dob")
    private Date dob;
    @Column(name="Address")
    private String address;
    @Column(name="UserName")
    private String userName;
    @Column(name="Password")
    private String password;
    @Column(name="Role")
    private Integer role;
    @Column(name="RegisterDOB") 
    private Date registerDob;

}
