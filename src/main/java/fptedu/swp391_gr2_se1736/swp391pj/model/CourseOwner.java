package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import javax.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class CourseOwner {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CourseOwnerID")
    private Integer courseOwnerId;
    @Column(name = "BeOwnDOB")
    private Date beOwnerBob;
    @Column(name = "Phone")
    private String phone;
    @Column(name = "Major")
    private String major;

    @Column(name = "UserID")
    private Integer userId;
}
