package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class FeedbackDetail {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FeedBackID")
    private Integer feedBackId;
    @Column(name = "FeedBackContent")
    private String feedBackContent;
    @Column(name = "FeedBackDate")
    private Date feedBackDate;

    @Column(name = "UserID")
    private Integer userId;
    @Column(name = "CourseID")
    private Integer courseId;

}
