package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Schedule {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ScheduleID")
    private Integer scheduleID;
    @Column(name = "TitleSchedule")
    private String titleSchedule;
    @Column(name = "SetTime")
    private Date setTime;
    @Column(name = "RemainderAfter")
    private Date reminderAfter;


    @Column(name = "CourseID")
    private Integer courseId;
    @Column(name = "UserID")
    private Integer userId;
}
