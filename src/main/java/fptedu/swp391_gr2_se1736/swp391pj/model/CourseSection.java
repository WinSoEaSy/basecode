package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class CourseSection {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CourseSectionID")
    Integer courseSectionId;
    @Column(name = "SectionTitle")
    String sectionTitle;
    @Column(name = "SectionDOB")
    Date sectionDob;
    @Column(name = "SectionOrder")
    Integer sectionOrder;

    @Column(name = "CourseID")
    Integer courseId;

}
