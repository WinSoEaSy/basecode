package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Material {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MaterialID")
    private Integer materialId;
    @Column(name = "Material_Link")
    private String material_link;
    @Column(name = "MaterialTitle")
    private String materialTitle;
    @Column(name = "MaterialName")
    private String materialName;
    @Column(name = "MaterialDOB")
    private Date materialDob;

    @Column(name = "CourseID")
    private Integer courseId;
}
