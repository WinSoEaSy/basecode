package fptedu.swp391_gr2_se1736.swp391pj.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class CourseProcess {
    
    @Column(name = "ContentStatus")
    private boolean contentStatus;

    @Column(name = "UserID")
    private Integer userId;
    @Column(name = "ContentID")
    private Integer contentId;
}
