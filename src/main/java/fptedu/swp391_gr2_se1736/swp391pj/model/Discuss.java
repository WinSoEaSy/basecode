package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Discuss {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CommentID")
    private Integer commentId;
    @Column(name = "Content")
    private String content;
    @Column(name = "PostDate")
    private Date postDate;
    @Column(name = "Vote")
    private Integer vote;

    @Column(name = "ParentID")
    private Integer parentId;
    @Column(name = "CourseID")
    private Integer courseId;
}
