package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Enroll {
    /*For the configuation, select attributes that need for your constructor (except ID) by using @nonNull anotation
     * If you want all, unlock the @AllArgConstructor
    */
    @Column(name = "EnrollDate")
    private Date enrollDate;
    @Column(name = "Checkout")
    private Double checkOut;
    @Column(name = "ProcessStatus")
    private boolean processStatus;

    @Column(name = "UserID")
    private Integer userId;
    @Column(name = "CourseID")
    private Integer courseId;

}
