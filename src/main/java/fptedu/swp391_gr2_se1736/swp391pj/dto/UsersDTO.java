package fptedu.swp391_gr2_se1736.swp391pj.dto;

import java.util.Date;

import org.springframework.stereotype.Component;

import fptedu.swp391_gr2_se1736.swp391pj.model.Users;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * UsersDTO
 */
@Getter
@Setter
@NoArgsConstructor
@Component
public class UsersDTO {
    private String fullName;
    private String email;
    private boolean gender;
    private Date dob;
    private String address;
    private String userName;
    private Integer role;
    private Date registerDob;


    public UsersDTO(Users u){
        this.fullName = u.getFullName();
        this.email = u.getEmail();
        this.dob = u.getDob();
        this.address = u.getAddress();
        this.userName = u.getUserName();
        this.role = u.getRole();
        this.registerDob = u.getRegisterDob();
        this.gender = u.isGender();
    }

    @Override
    public String toString() {
        return "UsersDTO [fullName=" + fullName + ", email=" + email + ", gender=" + gender + ", dob=" + dob
                + ", address=" + address + ", userName=" + userName + ", role=" + role + ", registerDob=" + registerDob
                + "]";
    }

    
}