package fptedu.swp391_gr2_se1736.swp391pj.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fptedu.swp391_gr2_se1736.swp391pj.dto.UsersDTO;
import fptedu.swp391_gr2_se1736.swp391pj.entity.loginform;
import fptedu.swp391_gr2_se1736.swp391pj.service.UserService;

@Controller
/* Modify your requestMapping */
@RequestMapping(path = "/users")  // the first path
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "") // http://localhost:8080/users?id=1
    /*@RequestParam anotation for declarce the request paramenter
     * for more https://www.baeldung.com/spring-request-param
     */
    public ModelAndView testGetUser(@RequestParam(name = "id", required = false) Integer id) {
        ModelAndView mv = new ModelAndView("home");// ModelAndView : set object/data request and set view that we send this data
        System.out.println("go to this func");
        UsersDTO usersDTO = userService.getUserById(id);
        mv.addObject("user", usersDTO);
        //return ModelAndView
        return mv;
    } 
    
    // move to another page
    @GetMapping("/login")  //url = users/login
    public ModelAndView loadLoginForm() {
        loginform lg = new loginform();
        ModelAndView mv = new ModelAndView("loginPage");
        //return the name of html page
        mv.addObject("loginform", lg);
        return mv;
    }


    //demo for login function ( get request form and send to another page)
    @PostMapping("/loginss")
    public ModelAndView demologinss(@RequestParam(name = "uname") String uname, @RequestParam(name = "upass") String upass){
        ModelAndView mv = new ModelAndView("demologinss");
        System.out.println("hahahahhahahahhahahhahahahahhah");
        System.out.println(uname);
        
        mv.addObject("uname", uname);
        mv.addObject("upass", upass);
        return mv;
    }

}
